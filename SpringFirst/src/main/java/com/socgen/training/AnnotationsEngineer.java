package com.socgen.training;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Component("antEngineer")

//@Controller("antEngineer")
//@Service
//@Repository
public class AnnotationsEngineer implements Engineer {

	// Used with the variable directly
//	@Autowired
//	@Qualifier("address2")
	Address address;
	
//	@Autowired
//	public AnnotationsEngineer(Address address) {
//		this.address =  address;
//	}

	public void work() {
		System.out.println("Annotations engineer has the address details : " + this.address.getLocality() + ", "
				+ this.address.getPinCode());

	}

	public Address getAddress() {
		return address;
	}

	//Autowire the bean address3 with the address here
//	@Autowired
//	@Qualifier("address3")
	
	@Resource(name="address3") //using a  JSR 250 Spec annotation here
	public void setAddress(Address address) {
		this.address = address;
	}
	
	//init method
	@PostConstruct
	void start() {
		System.out.println("Starting code goes here....");
	}
	
	//destroy lifecyle method
	@PreDestroy
	void runAtLast() {
		System.out.println("Run this method just before destroying");
	}
	
	
	

}
