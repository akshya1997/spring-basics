package com.socgen.training;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class NetworkEngineer implements Engineer{      //implements InitializingBean, DisposableBean {
	List<Address> addresses;

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
	public void work() {
		
		for(Address address: addresses) {
			System.out.println("The locations I have visited: " + address.getLocality() + ", " + address.getPinCode());
		}
	}

	public void destroy() throws Exception {
		System.out.println("Bean about to be destroyed...");
		
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("Bean initialized...");
		
	}
}
