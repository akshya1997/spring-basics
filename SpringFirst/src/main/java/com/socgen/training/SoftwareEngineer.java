package com.socgen.training;

import org.springframework.beans.factory.annotation.Required;

public class SoftwareEngineer implements Engineer {
	
	String name;
	int id;
	
	public Address getAddress() {
		return address;
	}

	@Required
	public void setAddress(Address address) {
		this.address = address;
	}

	Address address;
	
	public SoftwareEngineer(String name, int id) {
		System.out.println("SE constructor called...");
		this.name = name;
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
	
	public void work() {
		System.out.println("Hi, I am + " + this.getName() + " and my RegId is : " + this.getId()
		 + "and I live at " + this.address.getLocality() + ", " + this.address.getPinCode());
	}

}













//	
//	public void begin() {
//		System.out.println("SE initiliazed...");
//	}
//
//	public void work() {
//		System.out.println("Hi, I am + " + this.getName() + " and my RegId is : " + this.getId()
//		 + "and I live at " + this.address.getLocality() + ", " + this.address.getPinCode());
//	}
	
	
	
	
	
	
//}
